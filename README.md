# journald-errors

Repository for testing errors in libsystemd / journald. Each file is an example for an error.

Overview:

- [ ] [seek_head](https://github.com/systemd/systemd/issues/17662)
- [ ] [query_unique](https://github.com/systemd/systemd/issues/18075)
