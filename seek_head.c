// cc -g2 example_journal_reader.c -lsystemd
#include <stdio.h>
#include <time.h>
#include <errno.h>
#include <sys/poll.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <systemd/sd-journal.h>

int main(int argc, char *argv[])
{
  uint64_t prev_timestamp = 0;
  int r;
  sd_journal *j;

  r = sd_journal_open(&j, SD_JOURNAL_LOCAL_ONLY);
  if (r < 0)
  {
    fprintf(stderr, "Failed to open journal: %s\n", strerror(-r));
    return 1;
  }

  r = sd_journal_seek_head(j);
  if (r < 0)
  {
    fprintf(stderr, "Failed to seek to head of journal: %s\n", strerror(-r));
    return 1;
  }

  r = sd_journal_previous(j);
  if (r < 0)
  {
    fprintf(stderr, "Failed to get previous journal entry: %s\n", strerror(-r));
    return 1;
  }

  if (r > 0)
  {
    fprintf(stdout, "sd_journal_open --> sd_journal_seek_head --> sd_journal_previous: does not return EoF. \n");
  }
  sd_journal_close(j);

  r = sd_journal_open(&j, SD_JOURNAL_LOCAL_ONLY);
  if (r < 0)
  {
    fprintf(stderr, "Failed to open journal: %s\n", strerror(-r));
    return 1;
  }

  r = sd_journal_seek_head(j);
  if (r < 0)
  {
    fprintf(stderr, "Failed to seek to head of journal: %s\n", strerror(-r));
    return 1;
  }

  r = sd_journal_next(j);
  if (r < 0)
  {
    fprintf(stderr, "Failed to get next journal entry: %s\n", strerror(-r));
    return 1;
  }

  r = sd_journal_previous(j);
  if (r < 0)
  {
    fprintf(stderr, "Failed to get previous journal entry: %s\n", strerror(-r));
    return 1;
  }

  if (r > 0)
  {
    fprintf(stdout, "sd_journal_open --> sd_journal_seek_head --> sd_journal_next --> sd_journal_previous: does also not return EoF. \n");
  }

  if (r == 0)
  {
    fprintf(stdout, "sd_journal_open --> sd_journal_seek_head --> sd_journal_next --> sd_journal_previous: does return EoF, i.e. this works perfectly fine.\n");
  }

  sd_journal_close(j);

  return 0;
}